from control_work.lib import simpson_method, rectangle_method, trapezium_method, req

expected = 70.23218


def test_simpsons_method():
    result = simpson_method(req, 1, 10, 20, intervals=100)
    assert abs(result - expected) <= 0.1


def test_rectangle_method():
    result = rectangle_method(req, 1, 10, 20, intervals=100)
    assert abs(result - expected) <= 0.1


def test_trapezium_method():
    result = trapezium_method(req, 1, 10, 20, intervals=100)
    assert abs(result - expected) <= 0.1

import random
import pandas as pd
from control_work.lib import sma, ema, smm, sma_numpy

array = [random.uniform(-10, 10) for i in range(100)]


def test_correct_output_sma():
    expected = sma_numpy(array, 3)
    expected = [float("{:.6f}".format(item)) for item in expected]

    result = sma(array, 3)
    result = [float("{:.6f}".format(item)) for item in result]
    assert result == expected


def test_correct_output_ema():
    pd_array = pd.DataFrame(array)
    expected = pd.DataFrame.ewm(pd_array[0], span=3, adjust=False).mean().to_list()
    expected = [float("{:.6f}".format(item)) for item in expected]

    result = ema(array, 3)
    result = [float("{:.6f}".format(item)) for item in result]
    assert result == expected


def test_correct_output_smm():
    pd_array = pd.DataFrame(array)
    expected = pd_array.rolling(3, min_periods=1).median()[0].to_list()
    expected = [float("{:.6f}".format(item)) for item in expected]

    result = smm(array, 3)
    result = [float("{:.6f}".format(item)) for item in result]
    assert result == expected

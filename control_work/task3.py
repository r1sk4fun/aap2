import numpy.random as rnd
import numpy.linalg as nlg
import numpy as np
import matplotlib.pyplot as plt


def estimate_pi(n):
    x = np.zeros((n, 4))
    counter = 0
    for i in range(n):
        x[i, 0:2] = rnd.uniform(0.0, 1.0, 2)
        x[i, 2] = nlg.norm(x[i, :], ord=2)
        if x[i, 2] <= 1.0:
            counter += 1
        x[i, 3] = 4.0 * np.double(counter) / np.double(i + 1)
        print(i)
    return x


def plot_pi(n):
    x = estimate_pi(n)
    plt.semilogx(list(range(1, n+1)), x[:, 3])
    plt.xlabel('Number of simulated points')
    plt.ylabel('Approximate value of Pi')
    plt.title('Estimating Pi by Monte Carlo simulation')
    plt.show()


total = 1000000
plot_pi(total)

import statistics
import numpy as np


def rectangle_method(func, a, lower, upper, intervals=20):
    step = 1.0 * (upper - lower) / intervals
    integral = 0
    for i in range(intervals):
        integral += step * func(a, lower + i * step)
    return integral


def trapezium_method(func, a, lower, upper, intervals=20):
    step = 1.0 * (upper - lower) / intervals
    integral = 0.5 * (func(a, lower) + func(a, upper))
    for i in range(1, intervals):
        integral += func(a, lower + i * step)
    return integral * step


def simpson_method(func, a, lower, upper, intervals=20):
    if intervals % 2 == 1:
        intervals += 1
    step = 1.0 * (upper - lower) / intervals
    integral = (func(a, lower) + 4 * func(a, lower + step) + func(a, upper))
    for i in range(1, intervals // 2):
        integral += 2 * func(a, lower + (2 * i) * step) + 4 * func(a, lower + (2 * i + 1) * step)
    return integral * step / 3


def req(a, x):
    n = (9 * (7 * a ** 2 - 19 * a * x + 10 * x ** 2))
    d = (25 * a ** 2 + 30 * a * x + 9 * x ** 2)
    if d == 0:
        return None
    return n / d


def sma(data, w):
    """Simple Moving Average (SMA) Filter"""
    result = []
    for m in range(len(data) - (w - 1)):
        result.append(sum(data[m:m+w]) / w)
    return result


def ema(data, w):
    """Exponential Moving Average (EMA) Filter"""
    result = [data[0]]
    alpha = 2 / (w + 1)
    for m in range(len(data)):
        result.append((alpha * data[m]) + result[m] * (1 - alpha))
    return result[1::]


def smm(data, w):
    """Simple Moving Median (SMM) Filter"""
    result = [data[0]]
    for i in range(w - 2, 0, -1):
        result.append(statistics.median(data[0:w - i]))
    for m in range(len(data) - (w - 1)):
        result.append(statistics.median(data[m:m+w]))
    return result


def sma_numpy(data, w):
    return np.convolve(data, np.ones(w), 'valid') / w

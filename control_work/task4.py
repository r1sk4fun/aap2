import xlrd
from matplotlib import pyplot as plt
from control_work.lib import sma, ema, smm


file = xlrd.open_workbook('data.xls', formatting_info=True)
sheet = file.sheet_by_index(0)
data = sheet.col_values(8, start_rowx=1, end_rowx=10002)
data = [float(elem) for elem in data]

sma_result = sma(data, 30)
ema_result = ema(data, 30)
smm_result = smm(data, 30)

fig = plt.figure()
fig.set_size_inches(11.69, 8.27, forward=True)

plt.subplot(3, 1, 1)
plt.title('Simple Moving Average')
plt.plot(data, 'k', label='Before')
plt.plot(sma_result, 'y', label='After')
plt.legend()

plt.subplot(3, 1, 2)
plt.title('Exponential Moving Average')
plt.plot(data, 'k', label='Before')
plt.plot(ema_result, 'b', label='After')
plt.legend()

plt.subplot(3, 1, 3)
plt.title('Simple Moving Median')
plt.plot(data, 'k', label='Before')
plt.plot(smm_result, 'r', label='After')
plt.legend()

plt.tight_layout()
plt.show()

from matplotlib import pyplot as plt
from control_work import lib


def graphic(intervals, rectangle, trapezium, simpson):
    plt.plot(intervals, rectangle, '-or', label='Метод прямоугольников')
    plt.plot(intervals, trapezium, '-ob', label='Метод трапеций')
    plt.plot(intervals, simpson, '--oy', label='Метод Симпсона')
    plt.xlabel('количество интервалов')
    plt.ylabel('разность между истинным значением и приближением')
    plt.tight_layout()
    plt.legend()
    plt.show()


def get_values_array(method, func, k, low, up, ref, intervals):
    values_array = []
    for i in intervals:
        result = method(func, k, low, up, i)
        values_array.append(abs(ref - result))
    return values_array


intervals_array = [i for i in range(100, 1000, 100)]
expected = 70.23218
a = 1
x_low = 10
x_up = 20

rectangle_method = get_values_array(lib.rectangle_method, lib.req, a, x_low, x_up, expected, intervals_array)
trapezium_method = get_values_array(lib.trapezium_method, lib.req, a, x_low, x_up, expected, intervals_array)
simpson_method = get_values_array(lib.simpson_method, lib.req, a, x_low, x_up, expected, intervals_array)

graphic(intervals_array, rectangle_method, trapezium_method, simpson_method)

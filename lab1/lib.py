from random import randint
from lab1.util import counter


def bubble_sort(arr, reverse=False):
    n = len(arr)
    for i in range(n - 1):
        counter()
        for j in range(0, n - i - 1):
            counter()
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    if reverse:
        return arr[::-1]
    else:
        return arr


def shell_sort(arr, reverse=False):
    n = len(arr)
    gap = n // 2
    while gap > 0:
        for i in range(gap, n):
            counter()
            temp = arr[i]
            j = i
            while j >= gap and arr[j - gap] > temp:
                arr[j] = arr[j - gap]
                j -= gap
            arr[j] = temp
        gap //= 2
    if reverse:
        return arr[::-1]
    else:
        return arr


def insertion_sort(arr, reverse=False):
    for i in range(len(arr)):
        counter()
        cursor = arr[i]
        pos = i
        while pos > 0 and arr[pos - 1] > cursor:
            arr[pos] = arr[pos - 1]
            pos = pos - 1
        arr[pos] = cursor
    if reverse:
        return arr[::-1]
    else:
        return arr


def quick_sort(array, reverse=False):
    if len(array) < 2:
        return array
    low, same, high = [], [], []
    pivot = array[randint(0, len(array) - 1)]
    for item in array:
        counter()
        if item < pivot:
            low.append(item)
        elif item == pivot:
            same.append(item)
        elif item > pivot:
            high.append(item)
    arr = quick_sort(low) + same + quick_sort(high)
    if reverse:
        return arr[::-1]
    else:
        return arr

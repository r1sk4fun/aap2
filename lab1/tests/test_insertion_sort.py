from lab1.util import lists
from lab1.lib import insertion_sort


data = lists()


def test_ascending_order():
    result = insertion_sort(data[0], reverse=False)
    assert result == data[1]


def test_descending_order():
    result = insertion_sort(data[0], reverse=True)
    assert result == data[2]

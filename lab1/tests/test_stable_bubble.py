from lab1.util import lists, create_dict
from lab1.lib import bubble_sort


data = lists()


def test_stable_ascending():
    result = bubble_sort(data[0], reverse=False)
    assert create_dict(result) == create_dict(data[1])


def test_stable_descending():
    result = bubble_sort(data[0], reverse=True)
    assert create_dict(result) == create_dict(data[2])

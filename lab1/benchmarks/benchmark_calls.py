from lab1.util import create_array, line_with_calls, graphic
import lab1.lib
import cProfile
import pstats


first = 6 * 2
last = 6 * 20
step = 6 * 2

array_len = [i for i in range(first, last, step)]
operations = []

func = ['lab1.lib.bubble_sort(array)',
        'lab1.lib.insertion_sort(array)',
        'lab1.lib.shell_sort(array)',
        'lab1.lib.quick_sort(array)']

for e in func:
    for i in range(first, last, step):
        array = create_array(i)
        cProfile.run(e, 'stats.log')
        with open('output.txt', 'w') as log_file:
            p = pstats.Stats('stats.log', stream=log_file)
            p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
        f = open('output.txt')
        line = line_with_calls(f)
        f.close()
        operations.append(int(line))
    graphic(array_len, operations, 2, 'Количество операций')
    operations = []

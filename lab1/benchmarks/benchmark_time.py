from timeit import default_timer as timer
from lab1.util import create_array, graphic
import lab1.lib


first = 6 * 2
last = 6 * 20
step = 6 * 2

array_len = [i for i in range(first, last, step)]
array_time = []

func = [lab1.lib.bubble_sort,
        lab1.lib.insertion_sort,
        lab1.lib.shell_sort,
        lab1.lib.quick_sort]

for sort in func:
    for i in range(first, last, step):
        array = create_array(i)
        start = timer()
        sort(array)
        end = timer()
        array_time.append(end - start)
    graphic(array_len, array_time, 2, 'Время, с')
    array_time = []

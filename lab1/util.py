from random import randint
from matplotlib import pyplot as plt
from numpy import poly1d, polyfit

def lists():
    not_sorted_list = [(randint(0, 10), randint(0, 10)) for i in range(10)]
    sorted_list = sorted(not_sorted_list)
    sorted_list_reversed = sorted(not_sorted_list, reverse=True)
    return not_sorted_list, sorted_list, sorted_list_reversed


def create_dict(arr):
    result_dict = {k[0]: [] for k in arr}
    for index, num in arr:
        result_dict[index].append(num)
    return result_dict


def check_input(data):
    data = data.replace(' ', '')
    return data.isdigit()


def create_array(length):
    array = [(randint(1, 10), randint(1, 10)) for i in range(1, length + 1)]
    return array


def line_with_calls(file):
    result = 1
    lines = [i.strip() for i in file]
    lines_1 = [i for i in lines if i not in '' and i.split()[0].isnumeric() and not i.count('function')]
    for line in lines_1:
        if line.find('counter') != -1:
            result = line.split('   ')[0]
            break
    return result


def graphic(x_lst, y_lst, deg, ylabel):
    trend = poly1d(polyfit(x_lst, y_lst, deg))
    plt.subplot(1, 2, 1)
    plt.plot(x_lst, trend(x_lst), '-r', label='Теор.')
    plt.xlabel('Размер массива')
    plt.legend()
    plt.ylabel(ylabel)
    plt.subplot(1, 2, 2)
    plt.plot(x_lst, y_lst, '-b', label='Эксперем.')
    plt.xlabel('Размер массива')
    plt.tight_layout()
    plt.legend()
    plt.show()


def counter():
    return None

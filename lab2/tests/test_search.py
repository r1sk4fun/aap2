import lab2.lib_search


array = [0, 1, 4, 7, 10, 24, 37, 41, 72, 96]
search_str = 'ABADAACBADCAAABB'


def test_linear_search():
    result = lab2.lib_search.linear_search(array, 96)
    assert result == array.index(96)


def test_binary_search():
    result = lab2.lib_search.binary_search(array, 96)
    assert result == array.index(96)


def test_naive_search():
    result = lab2.lib_search.naive_search(search_str, 'ABB')
    assert result == search_str.find('ABB')


def test_kmp_search():
    result = lab2.lib_search.kmp_search(search_str, 'A')
    assert result == search_str.find('A')

import cProfile
import pstats
import lab2.lib_search
from lab1.util import line_with_calls, graphic
from lab2.util_search import create_array


first = 6 * 2
last = 6 * 20
step = 6 * 2

array_len = [i for i in range(first, last, step)]
operations = []

func = ['lab2.lib_search.linear_search(arrays[0], -1)',
        'lab2.lib_search.binary_search(arrays[0], -1)',
        'lab2.lib_search.naive_search(arrays[1], "FA")',
        'lab2.lib_search.kmp_search(arrays[1], "FA")']

for e in func:
    for i in range(first, last, step):
        arrays = create_array(i)
        cProfile.run(e, 'stats.log')
        with open('output.txt', 'w') as log_file:
            p = pstats.Stats('stats.log', stream=log_file)
            p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
        f = open('output.txt')
        line = line_with_calls(f)
        f.close()
        operations.append(int(line))
    graphic(array_len, operations, 2, 'Количество операций')
    operations = []

from random import randint
from string import ascii_uppercase
from random import choice


def create_array(length):
    array_int = sorted([randint(1, 100) for i in range(1, length + 1)])
    search_str = ''.join(choice(ascii_uppercase[0:4]) for i in range(length + 1))
    return array_int, search_str

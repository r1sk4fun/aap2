from lab1.util import counter


def linear_search(array, element):
    for i in range(len(array)):
        counter()
        if array[i] == element:
            return i
    return -1


def binary_search(array, element):
    left = 0
    right = len(array) - 1
    index = -1
    while (left <= right) and (index == -1):
        counter()
        mid = (left+right)//2
        if array[mid] == element:
            index = mid
        else:
            if element < array[mid]:
                right = mid - 1
            else:
                left = mid + 1
    return index


def naive_search(string, template):
    index = -1
    for i in range(len(string) - len(template) + 1):
        counter()
        success = True
        for j in range(len(template)):
            counter()
            if template[j] != string[i + j]:
                success = False
                break
        if success:
            index = i
            break
    return index


def prefix(string):
    p = [0] * len(string)
    j = 0
    i = 1
    while i < len(string):
        counter()
        if string[j] != string[i]:
            if j > 0:
                j = p[j - 1]
            else:
                i += 1
        else:
            p[i] = j + 1
            i += 1
            j += 1
    return p


def kmp_search(string, sub):
    k = 0
    i = 0
    p = prefix(sub)
    while k < len(string):
        counter()
        if sub[i] == string[k]:
            k += 1
            i += 1
            if i == len(sub):
                return k - len(sub)
        elif i > 0:
            i = p[i-1]
        else:
            k += 1
    return -1

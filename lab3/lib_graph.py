import networkx as nx
from lab1.util import counter


def graph_loader(data):
    """load graph data from .txt file"""
    with open(data, 'r') as gfile:
        nodes = tuple(gfile.readline().rstrip())
        edges = [tuple(e.rstrip()) for e in gfile]
    return nodes, edges


def get_nx_graph(graph_data):
    graph = nx.Graph()

    for node in graph_data[0]:
        graph.add_node(node)

    for u, v in graph_data[1]:
        graph.add_edge(u, v)

    return graph


def get_my_graph(nodes, edges):
    """create graph dictionary from given
    nodes and edges"""
    my_graph = {k: [] for k in nodes}

    for node, sec in edges:
        my_graph[node].append(sec)

    return my_graph


def find_all_paths(my_graph, start_vertex, end_vertex, path=[]):
    """ find all paths from start_vertex to
        end_vertex in graph """
    path = path + [start_vertex]
    if start_vertex == end_vertex:
        return [path]
    if start_vertex not in my_graph:
        return []
    paths = []
    for vertex in my_graph[start_vertex]:
        counter()
        if vertex not in path:
            extended_paths = find_all_paths(my_graph, vertex, end_vertex, path)
            for p in extended_paths:
                paths.append(p)
    return paths


def find_shortest_path(graph, start_vertex, end_vertex):
    """find shortest path using breadth-first search algorithm"""
    dist = {start_vertex: [start_vertex]}
    q = [start_vertex]
    while len(q):
        at = q.pop(0)
        for i in graph[at]:
            counter()
            if i not in dist:
                dist[i] = dist[at]+[i]
                q.append(i)
    return dist.get(end_vertex)

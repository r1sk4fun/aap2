import cProfile
import pstats
import networkx as nx
from matplotlib import pyplot as plt
from lab3.lib_graph import get_my_graph, find_all_paths
from lab1.util import line_with_calls


def graphic(x_lst, y_lst):
    plt.plot(x_lst, y_lst, '-b', label='Эксперем.')
    plt.ylabel('Количество операций')
    plt.xlabel('Размер массива')
    plt.tight_layout()
    plt.legend()
    plt.show()


first = 3
last = 24
step = 3

array_len = [i for i in range(first, last, step)]
operations = []

for i in range(first, last, step):
    G = nx.complete_graph(i)
    graph = get_my_graph(list(G.nodes()), list(G.edges()))
    cProfile.run('find_all_paths(graph, 0, i)', 'stats.log')
    with open('output.txt', 'w') as log_file:
        p = pstats.Stats('stats.log', stream=log_file)
        p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = line_with_calls(f)
    f.close()
    operations.append(int(line))
    print(i)
graphic(array_len, operations)
operations = []
